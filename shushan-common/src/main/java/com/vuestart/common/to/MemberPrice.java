/**
  * Copyright 2021 bejson.com 
  */
package com.vuestart.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2021-03-05 16:36:23
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class MemberPrice {

    private Long id;
    private String name;
    private BigDecimal price;

}