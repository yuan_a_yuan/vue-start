package com.vuestart.common.to.mq;

import com.vuestart.common.to.StockDetailTo;
import lombok.Data;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-24 9:46
 */
@Data
public class StockLockedTo {
    private Long id; // 库存工作单id
    private StockDetailTo detail; //
}
