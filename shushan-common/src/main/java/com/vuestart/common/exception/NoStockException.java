package com.vuestart.common.exception;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 17:12
 */
public class NoStockException extends RuntimeException {
    private Long skuId;
    public NoStockException() {}
    public NoStockException(String msg) {
        super(msg);
    }
    public NoStockException(Long skuId) {
        super("商品ID："+skuId+"没有足够的库存");
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
}
