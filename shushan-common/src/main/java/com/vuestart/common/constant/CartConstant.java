package com.vuestart.common.constant;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 9:54
 */
public class CartConstant {
    public static final String TEMP_USER_COOKIE_NAME = "user-key";
    public static final Integer TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30; // user-key 过期时间
}
