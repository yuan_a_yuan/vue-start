package com.vuestart.common.constant;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 15:29
 */
public class AuthServerConstant {

    public static final String SMS_CODE_CACHE_PREFIX = "sms:code";
    public static final String LOGIN_USER = "loginUser";

}
