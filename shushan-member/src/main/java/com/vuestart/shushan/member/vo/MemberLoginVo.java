package com.vuestart.shushan.member.vo;

import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 13:09
 */
@Data
public class MemberLoginVo {
    private String loginacct;
    private String password;
}
