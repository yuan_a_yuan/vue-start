package com.vuestart.shushan.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.member.entity.MemberReceiveAddressEntity;

import java.util.List;
import java.util.Map;

/**
 * 会员收货地址
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 20:26:54
 */
public interface MemberReceiveAddressService extends IService<MemberReceiveAddressEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 订单调用的远程服务
    List<MemberReceiveAddressEntity> getAddress(Long memberId);
}

