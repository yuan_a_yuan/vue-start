package com.vuestart.shushan.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.member.entity.MemberEntity;
import com.vuestart.shushan.member.exception.PhoneExistException;
import com.vuestart.shushan.member.exception.UserNameExistException;
import com.vuestart.shushan.member.vo.MemberLoginVo;
import com.vuestart.shushan.member.vo.MemberRegitVo;
import com.vuestart.shushan.member.vo.SocialUser;

import java.util.Map;

/**
 * 会员
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 20:26:55
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 用户注册
    void regist(MemberRegitVo vo);

    // 检查手机号和用户名是否唯一
    void checkPhoneUnique(String phone) throws PhoneExistException;
    void checkUserNameUnique(String userName) throws UserNameExistException;

    //用户登录
    MemberEntity login(MemberLoginVo vo);

    // 社交登录
    MemberEntity login(SocialUser socialUser) throws Exception;


}

