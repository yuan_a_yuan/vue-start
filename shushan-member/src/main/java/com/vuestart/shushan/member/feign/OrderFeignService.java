package com.vuestart.shushan.member.feign;

import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-24 17:10
 */
// todo 写错了。。。    @EnableFeignClients
@FeignClient("shushan-order")
public interface OrderFeignService {
    /**
     * 列表 远程调用  查询当前登录用户的所有订单信息
     */
    @PostMapping("/order/order/listWithItem")
    //@RequiresPermissions("order:order:list")
    R listWithItem(@RequestBody Map<String, Object> params);
}
