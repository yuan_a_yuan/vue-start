package com.vuestart.shushan.member.dao;

import com.vuestart.shushan.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 20:26:54
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
