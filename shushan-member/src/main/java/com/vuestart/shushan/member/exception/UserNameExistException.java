package com.vuestart.shushan.member.exception;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 12:01
 */
public class UserNameExistException extends RuntimeException {
    public UserNameExistException() {
        super("手机号已存在");
    }
}
