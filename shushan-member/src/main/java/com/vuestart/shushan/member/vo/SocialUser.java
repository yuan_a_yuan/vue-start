package com.vuestart.shushan.member.vo;

import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 16:54
 */
@Data
public class SocialUser {
    private String access_token;
    private String remind_in;
    private long expires_in;
    private String uid;
    private String isRealName;
}
