package com.vuestart.shushan.member.interceptor;

import com.vuestart.common.constant.AuthServerConstant;
import com.vuestart.common.vo.MemberRespVo;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 21:27
 */
@Component
public class LoginUserInterceptor implements HandlerInterceptor {

    public static ThreadLocal<MemberRespVo> loginUser = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {



        // /order/order/status/{orderSn}  过滤 这个请求 放行请求
        String uri = request.getRequestURI();
        boolean match = new AntPathMatcher().match("/member/**", uri);
        if (match){
            return true;
        }

        MemberRespVo attribute = (MemberRespVo) request.getSession().getAttribute(AuthServerConstant.LOGIN_USER);
        if (attribute != null) {
            loginUser.set(attribute);
            return true;
        }else {
            // 没登录就转到登录页
            request.getSession().setAttribute("msg", "请先进行登录");
            response.sendRedirect("http://auth.shushan.com/login.html");
            return false;
        }
    }
}
