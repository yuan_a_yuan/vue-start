package com.vuestart.shushan.member.exception;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 12:02
 */
public class PhoneExistException extends RuntimeException{
    public PhoneExistException() {
        super("用户已存在");
    }
}
