package com.vuestart.shushan.member.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 11:44
 */
@Data
public class MemberRegitVo {

    private String userName;

    private String password;

    private String phone;
}
