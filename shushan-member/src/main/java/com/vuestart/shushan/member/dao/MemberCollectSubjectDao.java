package com.vuestart.shushan.member.dao;

import com.vuestart.shushan.member.entity.MemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 20:26:55
 */
@Mapper
public interface MemberCollectSubjectDao extends BaseMapper<MemberCollectSubjectEntity> {
	
}
