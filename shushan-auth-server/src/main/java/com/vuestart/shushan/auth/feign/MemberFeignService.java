package com.vuestart.shushan.auth.feign;

import com.vuestart.common.utils.R;
import com.vuestart.shushan.auth.vo.SocialUser;
import com.vuestart.shushan.auth.vo.UserLoginVo;
import com.vuestart.shushan.auth.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 12:31
 */
@FeignClient("shushan-member")
public interface MemberFeignService {
    @PostMapping("/member/member/regist")
    R regist(@RequestBody UserRegistVo vo);

    // 用户登录
    @PostMapping("/member/member/login")
    R login(@RequestBody UserLoginVo vo);

    // 社交登录
    @PostMapping("/member/member/oauth2/login")
    R oauthlogin(@RequestBody SocialUser socialUser) throws Exception;
}
