package com.vuestart.shushan.auth.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 10:12
 */
@Data
public class UserRegistVo {
    @NotEmpty(message = "请输入用户名")
    @Length(min = 6, max = 18, message = "用户名必须在6-18位")
    private String userName;

    @NotEmpty(message = "请填写密码")
    @Length(min = 6, max = 18, message = "密码在6-18位")
    private String password;

    @NotEmpty(message = "请填写手机号")
    @Pattern(regexp = "^[1]([3-9)])[0-9]{9}$", message = "手机号格式不正确")
    private String phone;

//    @NotEmpty(message = "请输入验证码")
    private String code = "abcd";
}
