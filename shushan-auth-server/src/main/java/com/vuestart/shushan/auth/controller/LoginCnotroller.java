package com.vuestart.shushan.auth.controller;

import com.alibaba.fastjson.TypeReference;
import com.vuestart.common.constant.AuthServerConstant;
import com.vuestart.common.utils.R;
import com.vuestart.shushan.auth.feign.MemberFeignService;
import com.vuestart.common.vo.MemberRespVo;
import com.vuestart.shushan.auth.vo.UserLoginVo;
import com.vuestart.shushan.auth.vo.UserRegistVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * TODO 重定向携带数据 利用session原理 将数据岑入到session中 只要跳到下一个页面取出数据后 session中的数据就会清除掉 --》 一次性
 * Todo 分布式下的sessiong问题
 *
 * @创建：--> DKD <--
 * @日期：2021-04-21 8:46
 */
@Slf4j
@Controller
public class LoginCnotroller {
    // 短信验证码的第三方远程服务
    // 短信验证码的redis服务

    @Autowired
    MemberFeignService memberFeignService;

    // TODO 短信验证码功能


    //  注册功能
    @PostMapping("/regist")
    public String regist(@Valid UserRegistVo vo, BindingResult result, RedirectAttributes redirectAttributes) {
        vo.setCode("abcd");
        System.out.println("111111111111" + vo);
        if (result.hasErrors()) {
            Map<String, String> errors = result.getFieldErrors().stream().collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
            // model.addAttribute("errors", errors);
            redirectAttributes.addFlashAttribute("errors", errors);
            // type=Method Not Allowed, status=405

            // 校验出错，转到注册页
            return "redirect:http://auth.shushan.com/reg.html";
        }

        //真正注册 调用远程服务 --》 会员服务
        // 1、校验验证码
        String code = vo.getCode();
//        String code = "abcd";
//        vo.setCode("abcd");
//        System.out.println(vo.getCode());
        if (!StringUtils.isEmpty(code)) {
            // todo 判断验证码
            // 有验证码 调用远程服务
            R r = memberFeignService.regist(vo);
            if (r.getCode() == 0) {
                //成功

                return "redirect:http://auth.shushan.com/login.html";
            } else {
                Map<String, String> errors = new HashMap<>();
                errors.put("msg", r.getData("msg", new TypeReference<String>() {
                }));
                redirectAttributes.addFlashAttribute("errors", errors);
                return "redirect:http://auth.shushan.com/reg.html";

            }

        } else {
            Map<String, String> errors = new HashMap<>();
            errors.put("code", "验证码错误");
            redirectAttributes.addFlashAttribute("errors", errors);
            // 校验出错，转到注册页
            return "redirect:http://auth.shushan.com/reg.html";
        }
    }

    @PostMapping("/login")
    public String login(UserLoginVo vo, RedirectAttributes redirectAttributes, HttpSession session) {

        // 远程登录
        R login = memberFeignService.login(vo);
        if (login.getCode() == 0) {
            // 成功 返回首页
            MemberRespVo data = login.getData("data", new TypeReference<MemberRespVo>() {
            });
            // 将数据放入session中
            session.setAttribute(AuthServerConstant.LOGIN_USER, data);

            log.info("SSSSSSSSSS{}", data);

            return "redirect:http://shushan.com";
        }else {
            Map<String, String> errors = new HashMap<>();
            errors.put("msg", login.getData("msg", new TypeReference<String>(){}));
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:http://auth.shushan.com/login.html";
        }
    }
}
