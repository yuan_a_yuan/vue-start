package com.vuestart.shushan.auth.vo;

import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 12:58
 */
@Data
public class UserLoginVo {
    private String loginacct;
    private String password;
}
