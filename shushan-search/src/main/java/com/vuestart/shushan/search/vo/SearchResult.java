package com.vuestart.shushan.search.vo;

import com.vuestart.common.to.es.SkuEsModel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-19 18:10
 */
@Data
public class SearchResult {
    //查询到的所有商品信息
    private List<SkuEsModel> products;
    private Integer pageNum; // 以下是分页信息 当前页码
    private Long total; // 总共记录数

    /**
     * 总页码
     */
    private Integer totalPages;
    private List<Integer> pageNavs;
    /**
     * 当前查询到的结果，所有涉及的品牌
     */
    private List<BrandVo> brands;

    /**
     * 当前查询到的结果，所有涉及到的所有属性
     */
    private List<AttrVo> attrs;

    /**
     * 当前查询结果，所有涉及到的分类
     */
    private List<CatalogVo> catalogs;


    /**
     * 面包屑导航
     */
    ////////////////////////////// TODO  之一少 new ArrayList<>();
    private List<NavVo> navs = new ArrayList<>();
    private List<Long> attrIds = new ArrayList<>();

    @Data
    public static class NavVo{
        private String navName;
        private String navValue;
        private String link;
    }

    //==================以上是要返回给页面的所有信息
    @Data
    public static class BrandVo {
        /**
         * 品牌id
         */
        private Long brandId;
        /**
         * 品牌名字
         */
        private String brandName;
        /**
         * 品牌图片
         */
        private String brandImg;
    }

    // 返回属性集合
    @Data
    public static class AttrVo {
        /**
         * 属性id
         */
        private Long attrId;

        /**
         * 属性名字
         */
        private String attrName;
        /**
         * 属性值
         */
        private List<String> attrValue;
    }

    @Data
    public static class CatalogVo {
        /**
         * 分类id
         */
        private Long catalogId;
        /**
         * 品牌名字
         */
        private String CatalogName;
    }

}
