package com.vuestart.shushan.search.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.vuestart.common.valid.AddGroup;
import com.vuestart.common.valid.ListValue;
import com.vuestart.common.valid.UpdateGroup;
import com.vuestart.common.valid.UpdateStatusGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-26 9:30
 */
@Data
public class BrandVos {
    /**
     * 品牌id
     */
    private Long brandId;
    /**
     * 品牌名
     */
    private String name;
    /**
     * 品牌logo地址
     */
    private String logo;
    /**
     * 介绍
     */
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     */
    private Integer showStatus;
    /**
     * 检索首字母
     */

  private String firstLetter;
    /**
     * 排序
     */
    private Integer sort;
}
