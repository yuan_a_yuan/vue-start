package com.vuestart.shushan.search.vo;

import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-20 14:19
 */
@Data
public class BrandVo {
    /*
* "brandId": 0,
    "brandName": "string",
* */
    private Long brandId;
    private String brandName;
}
