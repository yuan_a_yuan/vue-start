package com.vuestart.shushan.search.vo;
// 检索页面相关

import lombok.Data;

import java.util.List;

/**
 * 封装页面所有可能传递的查询条件
 *
 * catalog3Id=342&keyword=对对&sort=sqleCount_desc
 */


@Data
public class SearchParam {
    private String keyword; // 全文匹配关键字
    private Long catalog3Id; // 三级分类id

    /**
     * sort=saleCout_asc/desc
     * sort=skuPrice_asc/desc
     * sort=hotScore_asc/desc
     * 排序条件
     */
    private String sort;

    /**
     * hasStock(是否有货) skuPrice区间，brandId、catalog3Id、attrs
     */

    //是否显示有货
    private Integer hasStock;

    // 价格区间
    private String skuPrice;

    //按照品牌进行查询，可以多选
    private List<Long> brandId;


    //按照属性进行筛选
    private List<String> attrs;

    // 页码
    private Integer pageNum = 1;

    private String _queryString; // 原
}
