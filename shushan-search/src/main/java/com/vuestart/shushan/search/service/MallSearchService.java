package com.vuestart.shushan.search.service;

import com.vuestart.shushan.search.vo.SearchParam;
import com.vuestart.shushan.search.vo.SearchResult;

public interface MallSearchService {

    /**
     * 检索的所有参数
     * @param param
     * @return
     */
    SearchResult search(SearchParam param);

}
