package com.vuestart.shushan.search;


import com.alibaba.fastjson.JSON;
import com.vuestart.shushan.search.config.ShushanElasticSearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

//@RunWith(SpringRunner.class) // --> 兼容性测试
@SpringBootTest
class ShushanSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;


    /**
     * Auto-generated: 2021-04-18 10:6:9
     *
     * @author json.cn (i@json.cn)
     * @website http://www.json.cn/java2pojo/
     */
    @ToString
    @Data
    public static class Account {

        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

    /**
     * 1、方便检索
     * {
     *     skuId: 1
     *     skuTitle: ”华为“
     *     price: 998
     *     saleCount: 99
     *     attrs: [
     *          {尺寸： 5寸}
     *          {CPU： 麒麟}
     *     ]
     * }
     * 冗余：
     *  100万*20 = 100000*2KB = 2000MB = 2G
     *
     *  2、sku索引{
     *      skuId：1
     *  }
     *  attr索引[
     *       {尺寸： 5寸}
     *       {CPU： 麒麟}
     *  ]
     *  搜索小米： 手机、粮食、电器
     *  10000个，4000个spu
     *  分步，4000个spu对应
     */


    /**
     * 1、测试存储数据到es
     * 更新也可以
     */
    @Test
    public void indexData() throws IOException {
        IndexRequest indexRequest = new IndexRequest("users");
        indexRequest.id("1"); // 文档的id
//        indexRequest.source("userName", "zhangsan",
//                "age", 18,
//                "gender", "男"
//        );
        User user = new User();
        user.setUserName("杨周");
        user.setAge(18);
        user.setGender("女");
        String jsonString = JSON.toJSONString(user);
        indexRequest.source(jsonString, XContentType.JSON); // 要保存的内容

        // 执行操作
        IndexResponse index = client.index(indexRequest, ShushanElasticSearchConfig.COMMON_OPTIONS);

        // 提取有用的相应数据
        System.out.println(index);

    }

    /**
     * 测试复杂检索请求
     * @throws IOException
     */
    @Test
    public void searchData() throws IOException {
        // 1、创建检索请求
        SearchRequest searchRequest = new SearchRequest();
        // 指定索引
        searchRequest.indices("bank");
        // 指定DSL。检索条件 domain specific language 领域专用语言 是一种json格式的查询语言
        // 封装的条件
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 1.1、构造检索条件
//        sourceBuilder.query();
//        sourceBuilder.from();
//        sourceBuilder.size();
//        sourceBuilder.aggregation();
        sourceBuilder.query(QueryBuilders.matchQuery("address", "mill"));

        // 1.2、按照年龄的分布聚合
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(100);
        sourceBuilder.aggregation(ageAgg);

        // 1.3、计算平均薪资
        AvgAggregationBuilder balanceAvg = AggregationBuilders.avg("balanceAvg").field("balance");
        sourceBuilder.aggregation(balanceAvg);


//        System.out.println("检索条件"+sourceBuilder.toString());
        searchRequest.source(sourceBuilder);

        // 2、执行检索
        SearchResponse searchResponse = client.search(searchRequest, ShushanElasticSearchConfig.COMMON_OPTIONS);

        // 3、分析结果 searchResponse
//        System.out.println(searchResponse.toString());
//        Map map = JSON.parseObject(searchResponse.toString(), Map.class);

        // 3.1、获取所有查到的结果
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit : searchHits ) {
//            hit.getIndex();
//            hit.getType();
            String string = hit.getSourceAsString();
            Account account = JSON.parseObject(string, Account.class);
            System.out.println("account："+ account);
        }

        // 3.2、拿到分析数据
        Aggregations aggregations = searchResponse.getAggregations();
//        for (Aggregation aggregation : aggregations.asList()) {
//            System.out.println("当前聚合名称："+ aggregation.getName());
//        }
        Terms ageAgg1 = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg1.getBuckets()) {
            String keyAsString = bucket.getKeyAsString();
            System.out.println("年龄："+ keyAsString+ "==>" + bucket.getDocCount()+"人");
        }

        Avg balanceAvg1 = aggregations.get("balanceAvg");
        System.out.println("平均薪资："+ balanceAvg1.getValue());
    }

    @Data
    static class User {
        private String userName;
        private String gender;
        private Integer age;
    }

    @Test
    public void contextLoads() {
        System.out.println(client);
    }

}
