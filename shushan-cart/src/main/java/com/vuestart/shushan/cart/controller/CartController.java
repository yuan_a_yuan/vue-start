package com.vuestart.shushan.cart.controller;

import com.vuestart.shushan.cart.service.CartService;
import com.vuestart.shushan.cart.vo.Cart;
import com.vuestart.shushan.cart.vo.CartItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 9:39
 */
@Slf4j
@Controller
public class CartController {

    @Autowired
    CartService cartService;

    // 订单远程调用的
    // todo 。。。。。。。。。。。。。。。。。。。。。。。。。
    @GetMapping("/currentUserCartItems")
    @ResponseBody
    public List<CartItem> currentUserCartItems() {
        return cartService.getUserCartItems();
    }

    // 购物车商品删除
    @GetMapping("/deleteItem")
    public String deleteItem (@RequestParam("skuId") Long skuId) {

        cartService.deleteItem(skuId);

        return "redirect:http://cart.shushan.com/cart.html";
    }

    // 购物车商品数量增减
    @GetMapping("/countItem")
    public String countItem (@RequestParam("skuId") Long skuId,@RequestParam("num") Integer num) {

        cartService.changeItemCount(skuId, num);

        return "redirect:http://cart.shushan.com/cart.html";
    }

    // 购物车选中与否状态处理
    @GetMapping("/checkItem")
    public String checkItem (@RequestParam("skuId") Long skuId,@RequestParam("check") Integer check) {

        cartService.checkItem(skuId, check);
        return "redirect:http://cart.shushan.com/cart.html";
    }

    /**
     * 浏览器有一个cookies： user-key;标识用户身份，一个月后过期
     * 如果第一次使用jd的购物车功能，都会带上一个 user-key
     * 浏览器以后保存，每次访问都会带上这个cookie
     *
     * 登录 session有
     * 没登录 按照cookie里面带deuser-key来做
     * 第一次 如果没有临时用户，则创建一个临时用户
     *
     * 拦截器
     * @return
     */
    @GetMapping("/cart.html")
    public String cartListPage (Model model) throws ExecutionException, InterruptedException {

       // 1、快速得到用户登录信息
//        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
//        log.info("信息{}",userInfoTo.toString());
        Cart cart = cartService.getCart();

        model.addAttribute("cart", cart);

        return "cartList";
    }

    /**
     * 添加商品到购物车
     * @return
     * RedirectAttributes ra 重定向携带数据
     *  ra.addAttribute() -->  将数据放在url后面
     *  ra.addFlashAttribute() --> 模拟session 一次性数据
     */
    @GetMapping("/addToCart")
    public String addToCart (@RequestParam("skuId") Long skuId,
                             @RequestParam("num") Integer num,
                             RedirectAttributes ra) throws ExecutionException, InterruptedException {

        cartService.addToCart(skuId, num);
//        model.addAttribute("item", cartItem);
//        model.addAttribute("skuId", skuId);
        ra.addAttribute("skuId", skuId);
        return "redirect:http://cart.shushan.com/addToCartSuccess.html";
    }

    /**
     * 跳转到成功页 放置多次刷新后多次提交
     * @param skuId
     * @param model
     * @return
     */
    @GetMapping("addToCartSuccess.html")
    public String addCartSuccessPage(@RequestParam("skuId") Long skuId, Model model) {
        // 重定向到成功页面 再次查询购物车
        CartItem item = cartService.getCartItem(skuId);
        model.addAttribute("item", item);
        return "success";
    }
}
