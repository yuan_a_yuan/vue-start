package com.vuestart.shushan.cart.config;

//import com.alibaba.fastjson.parser.ParserConfig;
//import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;


/**
 * @创建：--> DKD <--
 * @日期：2021-04-21 19:39
 */
@EnableRedisHttpSession
@Configuration
public class ShushanSessionConfig {

    @Bean
    public CookieSerializer cookieSerializer () {

        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();

        cookieSerializer.setDomainName("shushan.com"); // 扩大域级
        cookieSerializer.setCookieName("SHUSHANSESSION");


        return cookieSerializer;
    }

    @Bean
    public RedisSerializer<Object> springSessionDefaultRedisSerializer(){
        return  new GenericJackson2JsonRedisSerializer();
    }

}
