package com.vuestart.shushan.cart.service;

import com.vuestart.shushan.cart.vo.Cart;
import com.vuestart.shushan.cart.vo.CartItem;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 9:17
 */
public interface CartService {
    // 添加商品进购物车列表
    CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    // 重定向到成功页面 再次查询购物车
    CartItem getCartItem(Long skuId);

    // 获取整个购物车
    Cart getCart() throws ExecutionException, InterruptedException;

    // 清空购物车
    void clearCart(String cartkey);

    // 处理购物车中选中状态
    void checkItem(Long skuId, Integer check);

    // 修改购物车中数量
    void changeItemCount(Long skuId, Integer num);

    // 购物车商品删除
    void deleteItem(Long skuId);

    // 订单远程调用的
    List<CartItem> getUserCartItems();
}
