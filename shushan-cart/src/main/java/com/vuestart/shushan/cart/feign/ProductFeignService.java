package com.vuestart.shushan.cart.feign;

import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 11:22
 */
@FeignClient("shushan-product")
public interface ProductFeignService {

    /**
     * 信息 远程调用商品信息查询
     */
    @RequestMapping("/product/skuinfo/info/{skuId}")
    R getSkuInfo(@PathVariable("skuId") Long skuId);

    // 远程调用SkuSaleAttrValueController.java获取定制的sku信息
    @GetMapping("/product/skusaleattrvalue/stringlist/{skuId}")
    List<String> getSkuAttrValues(@PathVariable("skuId") Long skuId);

    // 订单调用的远程即时更新商品价格功能
    @GetMapping("product/skuinfo/{skuId}/price")
    R getPrice(@PathVariable("skuId") Long skuId);

}
