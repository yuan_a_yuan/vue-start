package com.vuestart.shushan.cart.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 9:51
 */
@ToString
@Data
public class UserInfoTo {
    private Long userId;
    private String userKey;

    private boolean tempUser = false; // 临时用户与否标识
}
