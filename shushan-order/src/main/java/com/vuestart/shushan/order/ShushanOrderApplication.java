package com.vuestart.shushan.order;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 1 引入 rabbitmq
 *  1映入 amqp场景
 */
@EnableAspectJAutoProxy(exposeProxy = true) // 动态代理 然后对外暴露代理对象 即使没有接口也可
@EnableFeignClients
@EnableRedisHttpSession
@EnableRabbit
@EnableDiscoveryClient
@SpringBootApplication
public class ShushanOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShushanOrderApplication.class, args);
    }

}
