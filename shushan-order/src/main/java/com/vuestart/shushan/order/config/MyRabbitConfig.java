package com.vuestart.shushan.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 17:36
 */
@Configuration
public class MyRabbitConfig {

    @Autowired
    RabbitTemplate rabbitTemplate;


    /*    @Bean
        @Primary
        public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory){
            RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
            this.rabbitTemplate = rabbitTemplate;
            rabbitTemplate.setMessageConverter(messageConverter());
            initRabbitTemplate();
            return rabbitTemplate;
        }*/


    @Bean
    public MessageConverter messageConverter() {

        /**
         * 使用JSON序列化机制，进行消息转换
         */
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplates
     */
    @PostConstruct // MyRabbitConfig 创建完成之后再执行这个方法
    public void initRabbitTemplate() {
        // 设置确认回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             * 只要消息抵达服务器 confirm 就会触发 ack 就会为 true
             * @param correlationData 当前消息的唯一关联数据 --》 是个唯一id
             * @param ack 消息是否成功收到
             * @param cause 失败原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                /**
                 * 1 保证好消息确认机制
                 * 2 每一个发送的消息都在数据库做好巨鹿 重试
                 *
                 */
                //  服务器收到了
                // 修改消息的状态
                System.out.println("confim...correlationData["+correlationData+"]===>"+"ack["+ack+"]===>"+"cause["+cause+"]");
            }
        });

        // 设置消息抵达队列的确认回调,
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             * 只要消息没有抵达给指定的队列，就触发这个失败回调
             * @param message 拖地失败的消息详细信息
             * @param replyCode 回复状态码
             * @param replyText 回复文本内容
             * @param exchange 消息发给哪个交换机的
             * @param routingKey 当时这个消息用哪个路由键
             */
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
                // 报错了， 修改数据库当前消息的状态-》错误
                System.out.println(
                        "消息投递失败["+message+"]===>" +
                                "回复码-replyCode["+replyCode+"]===>" +
                                "回复文本-replyText["+replyText+"]===>" +
                                "交换机-exchange["+exchange+"]===>" +
                                "路由键-routingKey["+routingKey+"]");
            }
        });
    }
}
