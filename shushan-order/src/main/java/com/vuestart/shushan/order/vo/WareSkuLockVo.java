package com.vuestart.shushan.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 16:28
 */
@Data
public class WareSkuLockVo {
    private String orderSn; //订单号

    private List<OrderItemVo> locks; // 需要锁住的所有商品信息

}
