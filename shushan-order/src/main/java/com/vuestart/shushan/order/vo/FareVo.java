package com.vuestart.shushan.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 14:53
 */
@Data
public class FareVo {
    private MemberAddressVo address;
    private BigDecimal fare;
}
