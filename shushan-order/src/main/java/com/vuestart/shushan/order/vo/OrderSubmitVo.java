package com.vuestart.shushan.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 13:50
 * 订单提交的VO
 */
@Data
public class OrderSubmitVo {
    private Long addrId;// 收货地址id
    private Integer payType; // 支付方式
    // 无需提交需要购买的商品，去购物车再获取一遍
    // 优惠、发票。。。

    private String orderToken; // 防重令牌
    private BigDecimal payPrice; // 应付价格 验价

    // 用户相关信息 直接去session中取

    private String note; // 订单备注
}
