package com.vuestart.shushan.order.feign;

import com.vuestart.shushan.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 22:48
 */
@FeignClient("shushan-cart")
public interface CartFeignService {
    // 订单远程调用的
    @GetMapping("/currentUserCartItems")
    List<OrderItemVo> currentUserCartItems();
}
