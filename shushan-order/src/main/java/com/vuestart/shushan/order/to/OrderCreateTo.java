package com.vuestart.shushan.order.to;

import com.vuestart.shushan.order.entity.OrderEntity;
import com.vuestart.shushan.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 14:39
 */
@Data
public class OrderCreateTo {
    private OrderEntity order;

    private List<OrderItemEntity> orderItems;

    private BigDecimal payPrice; // 订单应算价格

    private BigDecimal fare;// 运费


}
