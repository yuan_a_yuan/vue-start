package com.vuestart.shushan.order.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-20 22:55
 */
@ConfigurationProperties(prefix = "shushan.thread")
@Component
@Data
public class ThreadPoolConfigProperties {
    private Integer coreSize;
    private Integer maxSize;
    private Integer keepAliveTime;
}
