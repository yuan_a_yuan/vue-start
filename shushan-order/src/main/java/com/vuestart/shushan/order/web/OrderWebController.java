package com.vuestart.shushan.order.web;

import com.vuestart.common.exception.NoStockException;
import com.vuestart.shushan.order.service.OrderService;
import com.vuestart.shushan.order.vo.OrderConfirmVo;
import com.vuestart.shushan.order.vo.OrderSubmitVo;
import com.vuestart.shushan.order.vo.SubmitOrderResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 21:25
 */
@Slf4j
@Controller
public class OrderWebController {

    @Autowired
    OrderService orderService;

    /**
     * 订单确认需要的数据
     *
     * @param model
     * @return
     */
    @GetMapping("/toTrade")
    public String toTrade(Model model, HttpServletRequest request) throws ExecutionException, InterruptedException {

        OrderConfirmVo confirmVo = orderService.confirmOrder();
        model.addAttribute("orderConfirmData", confirmVo);
        return "confirm";
    }

    /**
     * 下单功能
     *
     * @param vo
     * @return
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes redirectAttributes) {

        try {
            SubmitOrderResponseVo responseVo = orderService.submitOrder(vo);
            // 下单 去创建订单 验证令牌，验证价格，锁库存
            // 下单成功 来到支付选择页
            // 下单失败 回到订单确认页 重新确认订单信息
//        System.out.println("订单提交的数据"+ vo);
            log.warn("eeeeeeeeeeeeeee{}", vo);
            if (responseVo.getCode() == 0) {
            // 下单成功 跳转支付页
            model.addAttribute("submitOrderResp", responseVo);
            return "pay";
            }else {
                String msg = "下单失败：";
                switch (responseVo.getCode()){
                    case 1: msg += "订单信息过期，请刷新后再次提交";break;
                    case 2: msg += "订单商品价格发生变化，请确认后再次提交"; break;
                    case 3: msg += "商品库存不足";break;
//                default: msg += "库存不足";
                }
                redirectAttributes.addFlashAttribute("msg", msg);
                // 跳转回订单确认页
                return "redirect:http://order.shushan.com/toTrade";
            }
        } catch (Exception e) {
            if (e instanceof NoStockException) {
                String message = e.getMessage();
                redirectAttributes.addFlashAttribute("msg", message);
            }
            return "redirect:http://order.shushan.com/toTrade";
        }
//        return null;
    }
}
