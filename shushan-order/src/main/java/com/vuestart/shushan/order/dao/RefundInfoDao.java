package com.vuestart.shushan.order.dao;

import com.vuestart.shushan.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:11:12
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {
	
}
