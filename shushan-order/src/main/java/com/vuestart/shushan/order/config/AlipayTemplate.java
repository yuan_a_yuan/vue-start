package com.vuestart.shushan.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.vuestart.shushan.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000117645208";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCibO7CYLYN5z2tprfkuf+3MWyIupqdHmBDxPk+I/HXyRUIUzUtolD6rERRKfYL16wVJ/wFvpV3zgPpobxBpGbANfBy5SezVa9HkkcBtm3QPmECMNpnMEczbQq8JspXl82YGnpcruVnqon25W7mnrTWFL0dYI5ASnTYE1vNGNDGxw3bwlCZWROkalkCKTLb8Q9TylMweLlKVCXjNtYBgbVgPx/I2GzbbIdNRonacQY4vLcz8TZweFxq/GNKR1c1UaBGrtc5bUlNtm+bTlaWiSOwmN7SKh8RnRRDJH+0sXaxhXhKWg0WnngkjNhJZZRVpL1RAl/9SxBSiU7OkWrqws/RAgMBAAECggEBAJH20O3PWWvPJid6zoF3AS2Q9dc90ppl5N6iEdZdGQXxR7xugjibSKiszMwfqGRmEdgFUb+b0NiURAHd/b8Dze7nykiGVJId62GnO7Z3SeI9XJJKcssOoL2/PUlulqH24qTRDqth5PivUkKVVn9bmmlUw8q1rcBq6OwRIYuDxIMMgv2L+8W7GSBiqYnPjVOqiY3uWLlmBLazMKba98siQuVM1O3TCeASzDBgIQRQqxnuDCsn+/p88vVDwy8U9OKUT4A+etNXO5KjyJ1IYxb+WnCaWwS72yDe3l2cz//tKo8lduB3CjY+qWJYT1flOWEHf/wtq+vEq0AsQ92w4WfP6d0CgYEA+cvbRak7sxmVgKHvLdId++IwW3feMI2lqAC7ivQWED3/rHYSo6p8DMOXwC8JFxYpTarafSxv9IW1xHCv/6w6xXo6BvgiY3EEidZ3JSgMyvH9QPyKTpiPsXJnlOjxZzGAFJCryZ+s7TXoMzGSq8iwfsAHX3IBCHsnoEfeffLWX+cCgYEApnWYF4XfcPvdSo2uDOaQhJ4DJY7xBBXJEOhs2/ec3f6/18XtD0/b542Cxom5gdasO0kYgDwYljRiDPqm1o/Kg040uIgMVx3Ma4wuXWsxfoj98xzDYldbRhlPXByUwsHlPboC7Qz1QaRjtM45BlDaPyH/SICKoajLqK8yyuwEO4cCgYB/IjTwaGh9B/liJYjAzl76Uq1/8CsClSUJs42/8+jCzXsyAEoOSHeoW2gBpS7ANERDRkC7UoGMdv0DBMNeof6rgUDFSUbfS6cerCupmo+gPrdBewa9Z8JCctKrJD+w4K2qSdNU49K0TCpo28MlcRf6+qsF9dT2xw9s8QcZHR3kYwKBgQCWT3uuShHRaW2TA8rWFWEMj87RnHjgsndkkCy0mKyK/Z3oTu7Ys8kkF3Guq/DaYKoXwtKgASRGhji/9ZqwBxncZcNPmnJiPCM6PlQjwUbpNvTyEdgQtkdSTkqOJ7ThBMtnaM95lQjmZHYwb0OpA1uARMfLhV8pyU1WHhYkjOjpgwKBgEphYhoh19tKoQa6qCUilxTgbNNeysI8ZUzYqfv/u32YZg+mvpV9HkgqrI7ljiIsp39ty0lfRemOB2wf033rC47hJDZMl53aCIjjegwtmEwePK90A9cnyq6j7/0ThP8cW/LQdxDFkxv/io20HSq6dPuM3cKWFkUElBU4IaH2eal5";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0QBDwjCNAV0YzuXRyPlDYjWmG1rwC8iM2x35E8DUdaMNt2J1bMj8jgUmqRHwJU3ZBN/qq+UdF9QeEZQvTlaFmfPbdLpf7AI220Ioz4iZ244rv8M2TV0rYochO33GoBOiB0zg66Lap5rns2zppGTcxfR525fi1AeC5XpjpmIp+PbzwplTVown0HJ6mPtIlupWOjzRk1DOrC2iS9mzise4HXJzoc84o0+T1AqAD+fiZlFc7c4raLECNKiKJFsLTH+bfvWZcXGFH2hrWvY8CuALTg+g2oldrZ1Wo0azx/H5LLkpUMtxPKWGTzmI7J6c36qLBdIjjTzZRO5JEcPQ9j8rvQIDAQAB";


    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url = "http://shushan.cn.utools.club/payed/notify";
//    private  String notify_url = "http://member.shushan.com/memberOrder.html";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
//    private  String return_url = "http://shushan.cn.utools.club/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";
    private  String return_url = "http://member.shushan.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 收单时间 -- 自添加
    private String timeout = "30m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+ timeout +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;
    }
}
