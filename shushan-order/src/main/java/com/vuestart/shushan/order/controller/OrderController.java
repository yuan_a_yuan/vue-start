package com.vuestart.shushan.order.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.vuestart.shushan.order.entity.OrderEntity;
import com.vuestart.shushan.order.service.OrderService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.common.utils.R;



/**
 * 订单
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:11:12
 */
@Slf4j
@RestController
@RequestMapping("order/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 远程调用，根据订单号查询订单状态
     * @param orderSn
     * @return
     */
    @GetMapping("/status/{orderSn}")
    public R getOrderStatus(@PathVariable("orderSn") String orderSn){
        OrderEntity orderEntity = orderService.getOrderByOrderSn(orderSn);
        return R.ok().setData(orderEntity);
    }

    /**
     * 远程调用  查询当前登录用户的所有订单信息
     */
    @PostMapping("/listWithItem")
    //@RequiresPermissions("order:order:list")
    public R listWithItem(@RequestBody Map<String, Object> params){
        PageUtils page = orderService.queryPageWithItem(params);

        return R.ok().put("page", page);
    }

    @GetMapping("/list")
    public R SSS (@RequestParam Map<String, Object> params) {

        PageUtils page = orderService.queryBaseOrderPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
//    @RequestMapping("/list")
//    //@RequiresPermissions("order:order:list")
//    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = orderService.queryPage(params);
//        log.warn("WWWWWWWWWWWWWWWWWWWWWWW"+page);
//        return R.ok().put("page", page);
//    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("order:order:info")
    public R info(@PathVariable("id") Long id){
		OrderEntity order = orderService.getById(id);

        return R.ok().put("order", order);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("order:order:save")
    public R save(@RequestBody OrderEntity order){
		orderService.save(order);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("order:order:update")
    public R update(@RequestBody OrderEntity order){
		orderService.updateById(order);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("order:order:delete")
    public R delete(@RequestBody Long[] ids){
		orderService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
