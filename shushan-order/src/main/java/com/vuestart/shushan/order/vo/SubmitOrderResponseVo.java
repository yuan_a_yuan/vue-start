package com.vuestart.shushan.order.vo;

import com.vuestart.shushan.order.entity.OrderEntity;
import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 14:15
 */
@Data
public class SubmitOrderResponseVo {
    private OrderEntity order;
    private Integer code; // 0成功 错误状态码

}
