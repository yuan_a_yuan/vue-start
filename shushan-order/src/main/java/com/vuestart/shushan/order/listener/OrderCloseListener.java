package com.vuestart.shushan.order.listener;

import com.rabbitmq.client.Channel;
import com.vuestart.shushan.order.entity.OrderEntity;
import com.vuestart.shushan.order.service.OrderService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-24 11:44
 */
@RabbitListener(queues = "order.release.order.queue")
@Service
public class OrderCloseListener {

    @Autowired
    OrderService orderService;

    @RabbitListener(queues = "order.release.order.queue")
    public void listener(OrderEntity entity, Channel channel, Message message) throws IOException {
        System.out.println("收到过期的订单信息，准备关闭订单"+entity.getOrderSn()+"===>"+entity.getId());
        try{
            // 关闭超时订单
            orderService.closeOrder(entity);
            // 手动调用支付宝收单

            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        }catch (Exception e){
            // 第二个参数是让消息重新回到队列
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
        }
    }
}
