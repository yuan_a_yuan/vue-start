package com.vuestart.shushan.order.feign;

import com.vuestart.shushan.order.vo.MemberAddressVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 22:15
 */
@FeignClient("shushan-member")
public interface MemberFeignService {
    // 订单调用的远程服务 会员地址列表 --------------------------------
    @GetMapping("/member/memberreceiveaddress/{memberId}/address")
    List<MemberAddressVo> getAddress(@PathVariable("memberId") Long memberId);

}
