package com.vuestart.shushan.order.vo;

import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 10:17
 */
@Data
public class SkuStockVo {
    private Long skuId;
    private Boolean hasStock;
}
