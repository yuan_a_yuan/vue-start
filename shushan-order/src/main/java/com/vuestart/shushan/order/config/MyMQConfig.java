package com.vuestart.shushan.order.config;

import com.rabbitmq.client.Channel;
import com.vuestart.shushan.order.entity.OrderEntity;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-24 0:45
 */
@Configuration
public class MyMQConfig {

    /**
     * 容器中的组件 都会自动创建
     * @return
     */
    @Bean("orderDelayQueue")
    public Queue orderDelayQueue(){
        Map<String, Object> arguments = new HashMap<>();

        arguments.put("x-dead-letter-exchange", "order-event-exchange");
        arguments.put("x-dead-letter-routing-key", "order.release.order");
        arguments.put("x-message-ttl", 60000); // todo 消息过期时间 60000ms 为 一分钟
        Queue queue = new Queue("order.delay.queue", true, false, false, arguments);

        return queue;
    }

    @Bean("orderReleaseOrderQueue")
    public Queue orderReleaseOrderQueue(){
        Queue queue = new Queue("order.release.order.queue", true, false, false);
        return  queue;
    }

    @Bean("orderEventExchange")
    public Exchange orderEventExchange(){
        // String name, boolean durable, boolean autoDelete, Map<String, Object> arguments
        TopicExchange topicExchange = new TopicExchange("order-event-exchange", true, false);
        return topicExchange;
    }

    @Bean
    public Binding orderCreateOrder(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        Binding binding = new Binding("order.delay.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.create.order",
                null);
        return binding;
    }

    @Bean
    public Binding orderReleaseOrder(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        Binding binding = new Binding("order.release.order.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.order",
                null);
        return binding;
    }

    /**
     * 订单释放直接和库存释放进行绑定
     * @return
     */
    @Bean
    public Binding orderReleaseOther(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        Binding binding = new Binding("stock.release.stock.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.other.#",
                null);
        return binding;
    }

}
