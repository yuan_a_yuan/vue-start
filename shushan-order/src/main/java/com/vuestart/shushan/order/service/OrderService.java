package com.vuestart.shushan.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.order.entity.OrderEntity;
import com.vuestart.shushan.order.vo.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:11:12
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 订单确认需要的数据
    OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;

    // 下单功能
    SubmitOrderResponseVo submitOrder(OrderSubmitVo vo);

    // 远程调用，根据订单号查询订单状态
    OrderEntity getOrderByOrderSn(String orderSn);

    // 关闭超时订单
    void closeOrder(OrderEntity entity);

    // 获取当前订单的支付信息
    PayVo getOrderPay(String orderSn);

    //列表 远程调用
    PageUtils queryPageWithItem(Map<String, Object> params);

    // 处理支付宝返回来的数据
    String handlerPayResult(PayAsyncVo vo);


    PageUtils queryBaseOrderPage(Map<String, Object> params);
}

