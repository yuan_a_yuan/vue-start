package com.vuestart.shushan.order.feign;

import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 15:35
 */
@FeignClient("shushan-product")
public interface ProductFeignService {
    // 远程通过skuId查询Spu信息
    @GetMapping("/product/spuinfo/skuId/{id}")
    R getSpuinfoBySkuId(@PathVariable("id") Long skuId);
}
