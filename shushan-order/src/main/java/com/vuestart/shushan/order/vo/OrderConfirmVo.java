package com.vuestart.shushan.order.vo;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 21:56
 * 订单确认需要的数据
 */
@Data
public class OrderConfirmVo {

    // 收货地址 `ums_member_receive_address`
    @Setter
    @Getter
    List<MemberAddressVo> address;

    // 所有选中的购物项
    @Setter
    @Getter
    List<OrderItemVo> items;

    // todo 发票信息

    // todo 优惠券
    @Setter
    @Getter
    Integer integration;

    @Setter
    @Getter
    Map<Long, Boolean> stocks;

    // 防重令牌
    @Setter
    @Getter
    String orderToken;

    public Integer getCount() {
        Integer i = 0;
        if (items != null) {
            for (OrderItemVo item : items) {
                i += item.getCount();
            }
        }
        return i;
    }

//    BigDecimal total; // 订单总额

    public BigDecimal getTotal() {
        BigDecimal sum = new BigDecimal("0");
        if (items != null) {
            for (OrderItemVo item : items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                sum = sum.add(multiply);
            }
        }
        return sum;
    }

//    BigDecimal payPrice; // 应付价格

    public BigDecimal getPayPrice() {
        return getTotal();
    }
}
