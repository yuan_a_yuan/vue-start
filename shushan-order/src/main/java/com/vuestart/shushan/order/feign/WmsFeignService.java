package com.vuestart.shushan.order.feign;

import com.vuestart.common.utils.R;
import com.vuestart.shushan.order.vo.WareSkuLockVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 10:13
 */
@FeignClient("shushan-ware")
public interface WmsFeignService {
    /**
     * 查询sku是否有库存 远程调用功能准备
     */
    @PostMapping("/ware/waresku/hasstock")
    R getSkusHasStock(@RequestBody List<Long> skuIds);

    /**
     * 根据收货地址计算运费运费
     * todo  /ware/waresku/ 写错了  还好有惊无险。。。。。。。。  要小心啊
     */
    @GetMapping("/ware/wareinfo/fare")
    R getFare(@RequestParam("addrId") Long addrId);

    /**
     * 远程调用，库存锁定
     *
     */
    @PostMapping("/ware/waresku/lock/order")
    R orderLockStock(@RequestBody WareSkuLockVo vo);
}
