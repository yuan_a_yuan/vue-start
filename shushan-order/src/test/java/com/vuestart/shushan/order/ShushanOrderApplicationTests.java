package com.vuestart.shushan.order;

import com.alibaba.fastjson.TypeReference;
import com.vuestart.common.utils.R;
import com.vuestart.shushan.order.entity.OrderEntity;
import com.vuestart.shushan.order.entity.OrderReturnReasonEntity;
import com.vuestart.shushan.order.feign.WmsFeignService;
import com.vuestart.shushan.order.vo.FareVo;
import com.vuestart.shushan.order.vo.OrderSubmitVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
//import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest
public class ShushanOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    WmsFeignService wmsFeignService;

    /*
    * 测试默认地址信息
    * */
    @Test
    public void addressTest() {
//        ThreadLocal<OrderSubmitVo> confirmVoThreadLocal = new ThreadLocal<>();
//        OrderSubmitVo submitVo = confirmVoThreadLocal.get();
        R fare = wmsFeignService.getFare(3L);
        FareVo fareRes = fare.getData(new TypeReference<FareVo>() {
        });
        log.info("地址测试-----------------》{}",fareRes.getAddress().getProvince());
        System.out.println(fareRes.getAddress().getProvince().equals("") ? "重庆市":fareRes.getAddress().getProvince());
    }

    /**
     * @Date: 2021/4/22
     * @Author:  --> DKD <--
     * 创建Exchange
     * 1、如何利用Exchange,Queue,Binding
     *      1、使用AmqpAdmin进行创建
     * 2、如何收发信息
     */
    @Test
    public void contextLoads() {
        /**
         * @Date: 2021/4/22
         * @Author:  --> DKD <--
         * //	public DirectExchange(
         * //	String name, 交换机的名字
         * //	boolean durable, 是否持久
         * //	boolean autoDelete, 是否自动删除
         * //	Map<String, Object> arguments)
         */
        DirectExchange directExchange = new DirectExchange("hello-java.exchange",true,false);
        amqpAdmin.declareExchange(directExchange);
        log.info("Exchange[{}]创建成功：","hello-java.exchange");
    }

    /**
     * 创建队列
     */
    @Test
    public void createQueue() {
        // public Queue(String name, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments) {
        Queue queue = new Queue("hello-java-queue",true,false,false);
        amqpAdmin.declareQueue(queue);
        log.info("Queue[{}]:","创建成功");
    }

    /**
     * 绑定队列
     */
    @Test
    public void createBinding() {
        // public Binding(String destination, 目的地
        // DestinationType destinationType, 目的地类型
        // String exchange,交换机
        // String routingKey,//路由键
        Binding binding = new Binding(
                "hello-java-queue",
                Binding.DestinationType.QUEUE,
                "hello-java.exchange",
                "hello.java",
                null);
        amqpAdmin.declareBinding(binding);
        log.info("Binding[{}]创建成功","hello-java-binding");
    }

    /**
     * @Date: 2021/4/22
     * @Author:  --> DKD <--
     * RabbitTemplate：消息发送处理组件
     */
    @Test
    public void sendMessageTest() {
        for(int i = 1; i <=5; i++) {
            if(i%2==0) {
                OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
                reasonEntity.setId(1L);
                reasonEntity.setCreateTime(new java.util.Date());
                reasonEntity.setName("哈哈");
                //
                String msg = "Hello World";
                // 发送的对象类型的消息，可以是一个json
                // 发送的对象类型的消息 会使用序列化机制
                rabbitTemplate.convertAndSend("hello-java.exchange","hello.java",reasonEntity, new CorrelationData(UUID.randomUUID().toString()));
            } else {
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setOrderSn(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend("hello-java.exchange","hello.java",orderEntity, new CorrelationData(UUID.randomUUID().toString()));
            }
            log.info("消息发送完成{}");
        }

    }

}
