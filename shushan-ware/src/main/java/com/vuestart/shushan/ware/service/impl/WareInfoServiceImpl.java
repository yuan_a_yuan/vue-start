package com.vuestart.shushan.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.vuestart.common.utils.R;
import com.vuestart.shushan.ware.feign.MemberFeignService;
import com.vuestart.shushan.ware.vo.FareVo;
import com.vuestart.shushan.ware.vo.MemberAddressVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.common.utils.Query;

import com.vuestart.shushan.ware.dao.WareInfoDao;
import com.vuestart.shushan.ware.entity.WareInfoEntity;
import com.vuestart.shushan.ware.service.WareInfoService;
import org.springframework.util.StringUtils;


@Service("wareInfoService")
public class WareInfoServiceImpl extends ServiceImpl<WareInfoDao, WareInfoEntity> implements WareInfoService {

    @Autowired
    MemberFeignService memberFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        QueryWrapper<WareInfoEntity> wareInfoEntityQueryWrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            wareInfoEntityQueryWrapper.eq("id", key).or()
                    .like("name", key).or()
                    .like("address", key).or()
                    .like("areacode", key);
        }

        IPage<WareInfoEntity> page = this.page(
                new Query<WareInfoEntity>().getPage(params),
                wareInfoEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    // 根据收货地址计算运费运费
    @Override
    public FareVo getFare(Long addrId) {
        FareVo fareVo = new FareVo();
        R r = memberFeignService.addrInfo(addrId);
//        if (r.getCode() == 0) {
            MemberAddressVo data = r.getData("memberReceiveAddress", new TypeReference<MemberAddressVo>() {
            });
            if (data != null) {
                String phone = data.getPhone();
                // 截取手机号当成运费信息
                String substring = phone.substring(phone.length() - 1, phone.length());
                BigDecimal fare = new BigDecimal(substring);
                fareVo.setAddress(data);
                fareVo.setFare(fare);
                return fareVo;
            }
//        }
        return null;
    }
}