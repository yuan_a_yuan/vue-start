package com.vuestart.shushan.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.rabbitmq.client.Channel;
import com.vuestart.common.exception.NoStockException;
import com.vuestart.common.to.OrderTo;
import com.vuestart.common.to.StockDetailTo;
import com.vuestart.common.to.mq.StockLockedTo;
import com.vuestart.common.utils.R;
import com.vuestart.shushan.ware.entity.WareOrderTaskDetailEntity;
import com.vuestart.shushan.ware.entity.WareOrderTaskEntity;
import com.vuestart.shushan.ware.feign.OrderFeignService;
import com.vuestart.shushan.ware.feign.ProductFeignService;
import com.vuestart.shushan.ware.service.WareOrderTaskDetailService;
import com.vuestart.shushan.ware.service.WareOrderTaskService;
import com.vuestart.shushan.ware.vo.OrderItemVo;
import com.vuestart.shushan.ware.vo.OrderVo;
import com.vuestart.shushan.ware.vo.SkuHasStockVo;
import com.vuestart.shushan.ware.vo.WareSkuLockVo;
import lombok.Data;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.common.utils.Query;

import com.vuestart.shushan.ware.dao.WareSkuDao;
import com.vuestart.shushan.ware.entity.WareSkuEntity;
import com.vuestart.shushan.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Resource
    WareSkuDao wareSkuDao;

    @Resource
    ProductFeignService productFeignService;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    WareOrderTaskService orderTaskService;

    @Autowired
    WareOrderTaskDetailService orderTaskDetailService;

    @Autowired
    OrderFeignService orderFeignService;


    // 解锁功能
    private void unLockStock(Long skuId, Long wareId, Integer num, Long taskDetailId) {
        // 库存解锁
        wareSkuDao.unlockStock(skuId, wareId, num);
        // 更新库存工作单的状态
        WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity();
        entity.setId(taskDetailId);
        entity.setLockStatus(2); // 已解锁
        orderTaskDetailService.updateById(entity);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        /*
         * skuId
         * wareId
         * */
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();
        String skuId = (String) params.get("skuId");
        if (!StringUtils.isEmpty(skuId)) {
            queryWrapper.eq("sku_id", skuId);
        }

        String wareId = (String) params.get("wareId");
        if (!StringUtils.isEmpty(wareId)) {
            queryWrapper.eq("ware_id", wareId);
        }


        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {

        // 1、如果还没库存记录，那就是新增操作
        List<WareSkuEntity> entities = wareSkuDao.selectList(new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId));

        if (entities == null || entities.size() == 0) {
            WareSkuEntity skuEntity = new WareSkuEntity();
            skuEntity.setSkuId(skuId);
            skuEntity.setStock(skuNum);
            skuEntity.setWareId(wareId);
            skuEntity.setStockLocked(0); // 锁定库存
            // TODO 1、 远程查询sku的名字，如果失败，整个事务无需回滚
            // 自己catch异常
            // TODO 2、 第二种办法 ---》   高级部分讲解
            try {
                R info = productFeignService.info(skuId);
                Map<String, Object> data = (Map<String, Object>) info.get("skuInfo");
                if (info.getCode() == 0) { // == 0 表示远程查询成功
                    skuEntity.setSkuName((String) data.get("skuName")); // 设置商品库存功能的sku_name
                }
            } catch (Exception e) {

            }
            wareSkuDao.insert(skuEntity);
        } else {
            this.baseMapper.addStock(skuId, wareId, skuNum);
        }
    }

    // WareSkuController --> WareSkuService
    @Override
    public List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds) {

        List<SkuHasStockVo> collect = skuIds.stream().map(skuId -> {
            SkuHasStockVo vo = new SkuHasStockVo();

            // 查询当前sku的总库存量
            // SELECT SUM(stock - stock_locked) FROM `wms_ware_sku` WHERE sku_id = 14
            Long count = baseMapper.getSkuStock(skuId);

            vo.setSkuId(skuId);
            vo.setHasStock(count == null ? false : count > 0);
            return vo;
        }).collect(Collectors.toList());
        log.warn("WareSkuServiceImpl" + collect);
        return collect;
    }

    // 远程调用，库存锁定

    /**
     * (rollbackFor = NoStockException.class)
     *
     * @param vo
     * @return 1 下单成功，订单过期没有支付被系统回收 接触库存锁定
     * 2
     */
    @Transactional
    @Override
    public Boolean orderLockStock(WareSkuLockVo vo) {
        /**
         * 保存库存工作单的详情
         * 追溯功能
         */
        WareOrderTaskEntity taskEntity = new WareOrderTaskEntity();
        taskEntity.setOrderSn(vo.getOrderSn());
        orderTaskService.save(taskEntity);

        // 1 按照下单的收货地址，找到一个就近仓库 锁定库存
        // 1 找到每个商品在哪个仓库中有库存
        List<OrderItemVo> locks = vo.getLocks();
        List<SkuWareHasStock> collect = locks.stream().map(item -> {
            SkuWareHasStock stock = new SkuWareHasStock();
            Long skuId = item.getSkuId();
            stock.setSkuId(skuId);
            stock.setNum(item.getCount());
            // 查询这个在哪里有库存
            List<Long> wareId = wareSkuDao.listWareIdHasSkuStock(skuId);
            stock.setWareId(wareId);
            return stock;
        }).collect(Collectors.toList());

        // 2 锁定库存
        for (SkuWareHasStock hasStock : collect) {
            Boolean skuStock = false;
            Long skuId = hasStock.getSkuId();
            List<Long> wareIds = hasStock.getWareId();
            if (wareIds == null && wareIds.size() == 0) {
                // 没有仓库有这个库存
                throw new NoStockException(skuId);
            }
            // 如果每一个商品都锁成功， 将当前商品锁定了几件的工作单发送给了mq
            // 如果锁定失败 ， 前面保存的工作单就回滚了 发送出去的消息 及时要解锁记录
            for (Long wareId : wareIds) {
                // 成功返回1 否则返回0
                Long count = wareSkuDao.lockSkuStock(skuId, wareId, hasStock.getNum());
                if (count == 1) {
                    skuStock = true;
                    // todo  库存锁定成功
                    WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity(null, skuId, "", hasStock.getNum(), taskEntity.getId(), wareId, 1);
                    orderTaskDetailService.save(entity);

                    StockLockedTo lockedTo = new StockLockedTo();
                    lockedTo.setId(taskEntity.getId());
                    StockDetailTo stockDetailTo = new StockDetailTo();
                    BeanUtils.copyProperties(entity, stockDetailTo);
                    // 只发id不行，放回滚以后找不到数据
                    lockedTo.setDetail(stockDetailTo);
                    rabbitTemplate.convertAndSend("stock-event-exchange", "stock.locked", lockedTo);
                    break;
                } else {
                    // 当前仓库锁失败，尝试下一个仓库
                }
            }
            if (!skuStock) {
                // 当前商品所有仓库都没锁
                throw new NoStockException(skuId);
            }
        }
        // 3 锁定成功
        return true;
    }

    @Override
    public void unlockStock(StockLockedTo to) {
        StockDetailTo detail = to.getDetail(); // 商品信息
        Long detailId = detail.getId();
        // 解锁
        // 1 查询数据库关于这个订单的库存锁定信息
        // 有 证明库存锁定成功了
        // 没有 库存锁定失败了 库存回滚 这种情况无需解锁
        WareOrderTaskDetailEntity byId = orderTaskDetailService.getById(detailId);
        if (byId != null) {
            // 解锁
            Long id = to.getId(); // 库存工作单的id
            WareOrderTaskEntity taskEntity = orderTaskService.getById(id);
            String orderSn = taskEntity.getOrderSn(); // 根据订单号查询订单状态
            // 远程调用 查询订单信息
            R r = orderFeignService.getOrderStatus(orderSn);
            if (r.getCode() == 0) {
                // 订单数据返回成功
                OrderVo data = r.getData(new TypeReference<OrderVo>() {
                });
                if (data == null || data.getStatus() == 4) {
                    // 订单不存在
                    // 订单被取消了  解锁库存
                    if (byId.getLockStatus() == 1) {
                        // 状态1 已锁定但未解锁
                        unLockStock(detail.getSkuId(), detail.getWareId(), detail.getSkuNum(), detailId);
                    }
                } else {
                    // 消息拒绝以后重新放回队列，让别人再继续消费解锁
                    throw new RuntimeException("远程服务失败");
                }
            }
        } else {
            // 无需解锁
        }
    }

    /**
     * 放置平淡服务卡顿 导致订单消息状态一直更改不了。查顶大年最新状态，会因将消息重放到队列中而一直尝试
     * @param orderTo
     */
    @Transactional
    @Override
    public void unlockStock(OrderTo orderTo) {
        String orderSn = orderTo.getOrderSn();
        // 查最新库存状态，放置重复解锁库存
        WareOrderTaskEntity task = orderTaskService.getOrderTaskByOrderSn(orderSn);
        Long id = task.getId();
        // 按照工作单找到所有没有解锁的库存进行解锁
        List<WareOrderTaskDetailEntity> entities = orderTaskDetailService.list(
                new QueryWrapper<WareOrderTaskDetailEntity>()
                        .eq("task_id", id)
                        .eq("lock_status", 1));
        for (WareOrderTaskDetailEntity entity : entities) {
            unLockStock(entity.getSkuId(), entity.getWareId(), entity.getSkuNum(), entity.getId());
        }
    }

    @Data
    static
    class SkuWareHasStock {
        private Long skuId;
        private Integer num;
        private List<Long> wareId;
    }


}