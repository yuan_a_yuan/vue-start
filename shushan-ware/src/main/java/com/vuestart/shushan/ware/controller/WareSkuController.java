package com.vuestart.shushan.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.vuestart.common.exception.BizCodeEnume;
import com.vuestart.common.exception.NoStockException;
import com.vuestart.shushan.ware.vo.SkuHasStockVo;
import com.vuestart.shushan.ware.vo.WareSkuLockVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.vuestart.shushan.ware.entity.WareSkuEntity;
import com.vuestart.shushan.ware.service.WareSkuService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.common.utils.R;



/**
 * 商品库存
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:19:03
 */
@Slf4j
@RestController
@RequestMapping("ware/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    /**
     * 远程调用，库存锁定
     *
     */
    @PostMapping("/lock/order")
    public R orderLockStock(@RequestBody WareSkuLockVo vo){
        try {
            Boolean stock = wareSkuService.orderLockStock(vo);
            return R.ok();
        }catch (NoStockException e) {
            return R.error(BizCodeEnume.NO_STOCK_EXCEPTION.getCode(), BizCodeEnume.NO_STOCK_EXCEPTION.getMsg());
        }
    }


    // 查询sku是否有库存 远程调用功能准备
//    @PostMapping("/hasstock")
//    public R<List<SkuHasStockVo>> getSkusHasStock(@RequestBody List<Long> skuIds) {
//        // sku_id, stock
//        List<SkuHasStockVo> vos = wareSkuService.getSkusHasStock(skuIds);
//
//        R<List<SkuHasStockVo>> ok = R.ok();
//        ok.setData(vos);
//        return ok;
//    }
//    @PostMapping("/hasstock")
//    public R getSkusHasStock(@RequestBody List<Long> skuIds) {
//        List<SkuHasStockVo> vos = wareSkuService.getSkusHasStock(skuIds);
//
//        R ok = R.ok();
//        ok.setData(vos);
//        log.warn("EEEEEEEEEEEEE"+ok.getData());
//        return ok;
//    }
    /**
     * 查询sku是否有库存
     *
     * @return
     */
    @PostMapping("/hasstock")
    public R getSkusHasStock(@RequestBody List<Long> skuIds){
        List<SkuHasStockVo> vos =  wareSkuService.getSkusHasStock(skuIds);
        return R.ok().setData(vos);
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:waresku:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("ware:waresku:info")
    public R info(@PathVariable("id") Long id){
		WareSkuEntity wareSku = wareSkuService.getById(id);

        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:waresku:save")
    public R save(@RequestBody WareSkuEntity wareSku){
		wareSkuService.save(wareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:waresku:update")
    public R update(@RequestBody WareSkuEntity wareSku){
		wareSkuService.updateById(wareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:waresku:delete")
    public R delete(@RequestBody Long[] ids){
		wareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
