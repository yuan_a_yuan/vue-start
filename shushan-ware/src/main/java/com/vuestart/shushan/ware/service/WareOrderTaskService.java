package com.vuestart.shushan.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.ware.entity.WareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:19:03
 */
public interface WareOrderTaskService extends IService<WareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 查最新库存状态，放置重复解锁库存
    WareOrderTaskEntity getOrderTaskByOrderSn(String orderSn);
}

