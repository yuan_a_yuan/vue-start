package com.vuestart.shushan.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 11:37
 */
@Data
public class FareVo {
    private MemberAddressVo address;
    private BigDecimal fare;
}
