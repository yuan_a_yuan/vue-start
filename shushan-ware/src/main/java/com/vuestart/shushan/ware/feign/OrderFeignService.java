package com.vuestart.shushan.ware.feign;

import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-24 10:28
 */
@FeignClient("shushan-order")
public interface OrderFeignService {
    /**
     * 远程调用，根据订单号查询订单状态
     * @param orderSn
     * @return
     */
    @GetMapping("/order/order/status/{orderSn}")
    R getOrderStatus(@PathVariable("orderSn") String orderSn);
}
