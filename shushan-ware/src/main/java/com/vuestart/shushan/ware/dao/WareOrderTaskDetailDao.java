package com.vuestart.shushan.ware.dao;

import com.vuestart.shushan.ware.entity.WareOrderTaskDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:19:03
 */
@Mapper
public interface WareOrderTaskDetailDao extends BaseMapper<WareOrderTaskDetailEntity> {
	
}
