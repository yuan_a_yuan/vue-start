package com.vuestart.shushan.ware.feign;

import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 11:04
 */
@FeignClient("shushan-member")
public interface MemberFeignService {
    /**
     * 获取收货地址信息
     */
    @RequestMapping("/member/memberreceiveaddress/info/{id}")
    //@RequiresPermissions("member:memberreceiveaddress:info")
    R addrInfo(@PathVariable("id") Long id);
}
