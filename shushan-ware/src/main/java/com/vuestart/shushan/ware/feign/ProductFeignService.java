package com.vuestart.shushan.ware.feign;

import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("shushan-product")
public interface ProductFeignService {

    /**
     *   /api/prodoct/skuInfo/info/{skuId}
     *   /product/skuinfo/info/{skuId}
     *
     *   1、让所有请求过网关
     *      1、@FeignClient("shushan-product")：给shushan-gateway所在的机器发请求
     *      2、/api/prodoct/skuInfo/info/{skuId}
     *   2、直接让后台指定服务处理
     *      1、@FeignClient("shushan-product")：
     *      2、/product/skuinfo/info/{skuId}
     * @return
     */
    @RequestMapping("/product/skuinfo/info/{skuId}")
    //@RequiresPermissions("product:skuinfo:info")
    public R info(@PathVariable("skuId") Long skuId);
}
