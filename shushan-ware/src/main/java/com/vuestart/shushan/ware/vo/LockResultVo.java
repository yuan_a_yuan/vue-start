package com.vuestart.shushan.ware.vo;

import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期： 2021-04-23 16:34
 */
@Data
public class LockResultVo {
    private Long skuId;
    private Integer num;
    private boolean locked;
}
