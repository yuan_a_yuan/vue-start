package com.vuestart.shushan.ware.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-22 22:07
 */
@Data
public class OrderItemVo {
    private Long skuId;
    private String title;
    private String image;
    private List<String> skuAttr;
    private BigDecimal price;
    private Integer count;
    private BigDecimal totalPrice;

    private BigDecimal weight; // 重量
}
