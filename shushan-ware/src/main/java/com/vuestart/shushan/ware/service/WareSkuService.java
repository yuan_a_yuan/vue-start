package com.vuestart.shushan.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.to.OrderTo;
import com.vuestart.common.to.mq.StockLockedTo;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.ware.entity.WareSkuEntity;
import com.vuestart.shushan.ware.vo.LockResultVo;
import com.vuestart.shushan.ware.vo.SkuHasStockVo;
import com.vuestart.shushan.ware.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:19:03
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds);

    //远程调用，库存锁定
    Boolean orderLockStock(WareSkuLockVo vo);

    void unlockStock(StockLockedTo to);

    void unlockStock(OrderTo orderTo);
}

