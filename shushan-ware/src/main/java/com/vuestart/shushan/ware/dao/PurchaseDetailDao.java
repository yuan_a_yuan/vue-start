package com.vuestart.shushan.ware.dao;

import com.vuestart.shushan.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:19:03
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
