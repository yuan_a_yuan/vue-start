package com.vuestart.shushan.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.ware.entity.WareInfoEntity;
import com.vuestart.shushan.ware.vo.FareVo;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 仓库信息
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 22:19:03
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 根据收货地址计算运费运费
    FareVo getFare(Long addrId);
}

