package com.vuestart.shushan.product.vo;

import lombok.Data;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-20 21:41
 */
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
