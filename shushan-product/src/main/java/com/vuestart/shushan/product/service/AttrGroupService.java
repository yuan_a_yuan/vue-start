package com.vuestart.shushan.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.product.entity.AttrGroupEntity;
import com.vuestart.shushan.product.vo.AttrGroupWithAttrsVo;
import com.vuestart.shushan.product.vo.SkuItemVo;
import com.vuestart.shushan.product.vo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 15:58:32
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId);

    List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId);

    //         // 5、获取spu规格参数信息
    List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId);
}

