/**
  * Copyright 2021 bejson.com 
  */
package com.vuestart.shushan.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2021-03-05 16:36:23
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Images {

    private String imgUrl;
    private int defaultImg;

}