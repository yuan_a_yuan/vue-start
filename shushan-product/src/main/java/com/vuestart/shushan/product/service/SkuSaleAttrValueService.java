package com.vuestart.shushan.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.product.entity.SkuSaleAttrValueEntity;
import com.vuestart.shushan.product.vo.SkuItemSaleAttrVo;
import com.vuestart.shushan.product.vo.SkuItemVo;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 15:58:32
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //        // 3、获取spu销售属性组合
    List<SkuItemSaleAttrVo> getSaleAttrsBySpuId(Long spuId);
    // 3、获取sku销售属性组合
    List<String> getSkuAttrValuesAsStringList(Long skuId);
}

