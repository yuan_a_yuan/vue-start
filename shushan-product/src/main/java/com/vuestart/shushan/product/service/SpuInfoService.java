package com.vuestart.shushan.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.product.entity.SpuInfoDescEntity;
import com.vuestart.shushan.product.entity.SpuInfoEntity;
import com.vuestart.shushan.product.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 15:58:32
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVo vo);

    void saveBaseSpuInfo(SpuInfoEntity infoEntity);

    PageUtils queryPageByCondition(Map<String, Object> params);

    void up(Long spuId);

    // 远程通过skuId查询Spu信息
    SpuInfoEntity getSpuInfoBySkuId(Long skuId);


    /**
     * 商品上架
     * @param spuId
     */
//    void up(Long spuId);
}

