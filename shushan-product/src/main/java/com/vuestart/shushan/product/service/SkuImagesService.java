package com.vuestart.shushan.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.product.entity.SkuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * sku图片
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 15:58:32
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 详情页图片信息
    List<SkuImagesEntity> getImagesBySkuId(Long skuId);
}

