package com.vuestart.shushan.product.web;

import com.vuestart.shushan.product.entity.CategoryEntity;
import com.vuestart.shushan.product.service.CategoryService;

import com.vuestart.shushan.product.vo.Catelog2Vo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    @Resource
    CategoryService categoryService;

    @GetMapping({"/","/index.html"})
    public String indexPage(Model model) {
        // todo 1、查出所有一级分类
        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
        model.addAttribute("categorys", categoryEntities);
        return "indexAnt";
    }

    @GetMapping("/ant")
    public String listPage(Model model) {

        // todo 1、查出所有一级分类
        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
        System.out.println(categoryEntities);
        model.addAttribute("categorys", categoryEntities);

        return "index";
    }

    // index/json/catalog.json
    @ResponseBody
    @GetMapping("/index/json/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        Map<String, List<Catelog2Vo>> catalogJson = categoryService.getCatalogJson();
        return catalogJson;
    }
}
