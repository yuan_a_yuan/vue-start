package com.vuestart.shushan.product.feign;

import com.vuestart.common.to.SkuHasStockVo;
import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
// P132 9:39
@FeignClient("shushan-ware")
public interface WareFeignService {
    /**
     * 1、R设计的时候可以加上泛型
     * 2、直接返回想要的结果
     * 3、自己封装解析的结果
     * @param skuIds
     * @return
     */
//    @PostMapping("/ware/waresku/hasstock")
//    R<List<SkuHasStockVo>> getSkusHasStock(@RequestBody List<Long> skuIds);
    @PostMapping("/ware/waresku/hasstock")
    R getSkusHasStock(@RequestBody List<Long> skuIds);
}
