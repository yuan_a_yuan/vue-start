package com.vuestart.shushan.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 二级分类
@NoArgsConstructor  // 无参构造器
@AllArgsConstructor // 全参构造器
@Data // getter & setter
public class Catelog2Vo {
    private String catalog1Id; // 一级
    private List<CateLog3Vo> catalog3List; //三级分类
    private String id;
    private String name;


    /**
     *三级分类VO
     "catalog2Id":"1",
     "id":"1",
     "name":"电子书"
     */
    @NoArgsConstructor  // 无参构造器
    @AllArgsConstructor // 全参构造器
    @Data // getter & setter
    public static class CateLog3Vo{
        private String catalog2Id;
        private String id;
        private String name;

    }
}
