package com.vuestart.shushan.product.dao;

import com.vuestart.shushan.product.entity.SkuSaleAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vuestart.shushan.product.vo.SkuItemSaleAttrVo;
import com.vuestart.shushan.product.vo.SkuItemVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sku销售属性&值
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 15:58:32
 */
@Mapper
public interface SkuSaleAttrValueDao extends BaseMapper<SkuSaleAttrValueEntity> {

    List<SkuItemSaleAttrVo> getSaleAttrsBySpuId(@Param("spuId") Long spuId);

    // 3、获取sku销售属性组合
    List<String> getSkuAttrValuesAsStringList(@Param("skuId") Long skuId);

}
