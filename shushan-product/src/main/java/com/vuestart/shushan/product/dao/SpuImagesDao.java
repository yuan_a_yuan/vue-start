package com.vuestart.shushan.product.dao;

import com.vuestart.shushan.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 15:58:32
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
