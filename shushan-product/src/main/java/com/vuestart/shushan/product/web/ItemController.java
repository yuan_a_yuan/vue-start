package com.vuestart.shushan.product.web;

import com.vuestart.shushan.product.service.SkuInfoService;
import com.vuestart.shushan.product.vo.SkuItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.ExecutionException;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-20 16:49
 */
@Controller
public class ItemController {

    @Autowired
    SkuInfoService skuInfoService;

    /***
     * @Date: 2021/4/20
     * @Author:  --> DKD <--
     * 展示当前sku的详情
     */
    @GetMapping("/{skuId}.html")
    public String skuItem (@PathVariable("skuId") Long skuId, Model model) throws ExecutionException, InterruptedException {

        System.out.println("准备查询"+skuId+"的详情");

        SkuItemVo vo = skuInfoService.item(skuId);
        model.addAttribute("item", vo);

        return "itemAnt";
    }

    @GetMapping("/ant/{skuId}.html")
    public String skuItems (@PathVariable("skuId") Long skuId, Model model) throws ExecutionException, InterruptedException {

        System.out.println("准备查询"+skuId+"的详情");

        SkuItemVo vo = skuInfoService.item(skuId);
        model.addAttribute("item", vo);

        return "item";
    }
}
