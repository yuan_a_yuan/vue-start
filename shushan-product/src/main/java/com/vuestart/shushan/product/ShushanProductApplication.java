package com.vuestart.shushan.product;

import com.vuestart.shushan.product.service.BrandService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/*
* 1.整合mybatis-plus
*   1）导入依赖
*   <dependency>
*        <groupId>com.baomidou</groupId>
*        <artifactId>mybatis-plus</artifactId>
*        <version>3.2.0</version>
*    </dependency>
*    2）配置
*       ①配置数据源 ；
*           一、导入数据库驱动。
*           二、在application.yml配置数据源相关信息
*       ②配置Mybatis-plus；
*           一、使用@MapperScan
*           二、告诉Mybatis-plus sql映射文件位置
*
* 2、逻辑删除
*   1)配置全局的逻辑删除规则（可以省略）
*   2)配置逻辑删除组件Bean（3.1及以上版本省略）
*   3)给Bean加上逻辑删除注解@TableLogic
*
* 3、JSR303
*   1)给Bean添加校验注解jakarta.validation.constraints，并定义自己的message提示
*   2)开启校验功能 @Valid，在需要校验的地方加上，效果是校验错误后会有默认的响应
*   3)给校验的Bean后紧跟一个BindingResult，就可以获取到校验的结果
*   4)分组校验（多场景的复杂校验）
*       .1、@NotBlank(message = "品牌名不能为空", groups = {AddGroup.class, UpdateGroup.class})
*           给校验注解标注什么情况需要校验
*       .2、@Validated(AddGroup.class)
*       .3、默认没有指定分组的校验@NotBlank，在分组校验情况下不生效，只会在@Validated中生效
*   5)自定义校验
*      .1、编写一个自定义的校验注解
*      .2、编写一个自定义校验器
*      .3、关联自定义的校验器和自定义的校验注解
*
* 4、统一异常处理
* @ControllerAdvice
*   1)编写异常处理类，使用@ControllerAdvice
*   2)使用@ExceptionHandler标注方法可以处理的异常
*
* 5、引入模板引擎
*     .1、thyemleat-start: 关闭缓存
*     .2、静态资源都放在static文件夹下，可以按照路径访问
*     .3、页面放在templates下，直接访问
*           Springboot，访问项目的时候，默认会找index
*     .4、页面修改实时更新（不重启服务的情况下）
*           引入dev-tools
*           修改完页面，只需ctrl+shift+f9重新构建页面即可，如果是代码配置，还是重启服务
* 6、整合redis
*   1、引入redis依赖 data-redis-starter
*   2、简单配置redis的host等信息
*   3、使用springboot自动配置好的template
* */

@EnableRedisHttpSession
@EnableFeignClients(basePackages = "com.vuestart.shushan.product.feign")
@EnableDiscoveryClient
@MapperScan("com.vuestart.shushan.product.dao")
@SpringBootApplication
public class ShushanProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShushanProductApplication.class, args);
    }

}
