//package com.vuestart.shushan.product.web;
//
//import com.vuestart.shushan.product.entity.CategoryEntity;
//import com.vuestart.shushan.product.service.CategoryService;
//import com.vuestart.shushan.product.vo.Catelog2Vo;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.annotation.Resource;
//import java.util.List;
//import java.util.Map;
//
///**
// * @创建：--> DKD <--
// * @日期：2021-04-22 20:03
// */
//@Controller
//public class HelloController {
//
//    @Resource
//    CategoryService categoryService;
//
//    @GetMapping("/ant")
//    public String listPage(Model model) {
//
//        // todo 1、查出所有一级分类
//        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
//        model.addAttribute("categorys", categoryEntities);
//
//        return "indexAnt";
//    }
//
//}
