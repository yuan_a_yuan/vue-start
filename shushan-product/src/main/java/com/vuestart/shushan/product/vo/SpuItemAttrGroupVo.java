package com.vuestart.shushan.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-20 19:38
 */
@ToString
@Data
public class SpuItemAttrGroupVo {
    private String groupName;
    List<Attr> attrs;
}
