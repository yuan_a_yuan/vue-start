package com.vuestart.shushan.product.vo;

import lombok.Data;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-20 19:40
 */
@Data
public class SkuItemSaleAttrVo {
    private Long attrId;
    private String attrName;
    /////////////////////////////////////////////////////// todo ......
//    private String attrValues;
    private List<AttrValueWithSkuIdVo> attrValues;
}
