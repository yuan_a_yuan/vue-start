package com.vuestart.shushan.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.common.utils.Query;
import com.vuestart.shushan.product.dao.CategoryDao;
import com.vuestart.shushan.product.entity.CategoryEntity;
import com.vuestart.shushan.product.service.CategoryBrandRelationService;
import com.vuestart.shushan.product.service.CategoryService;
import com.vuestart.shushan.product.vo.Catelog2Vo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Resource
    CategoryBrandRelationService categoryBrandRelationService;

//    @Resource
//    StringRedisTemplate redisTemplate;
    @Autowired
//    @Resource ////////////// TODO 引发的血案。。。。。
    StringRedisTemplate redisTemplate;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        // 1、查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);

        // 2、组装成父子的树形结构
        // 1)、找到所有的一级分类


        return entities.stream().filter(categoryEntity ->
            categoryEntity.getParentCid() == 0
        ).peek((menu) -> menu.setChildren(getChildrens(menu, entities))).sorted(Comparator.comparingInt(menu -> (menu.getSort() == null ? 0 : menu.getSort()))).collect(Collectors.toList());
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {
        // TODO 1、检查当前删除的菜单，是否被别的地方引用

        // 逻辑删除
        baseMapper.deleteBatchIds(asList);
    }

    // [1,25,225]
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        List<Long> parentPath = findParentPath(catelogId, paths);

        Collections.reverse(parentPath);

        return parentPath.toArray(new Long[0]);
    }

    /**
     *级联更新所有关联的数据
     * */
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);// 级联更新
        categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());

    }

    // web --> IndexController --> CategoryService.java  查出所有一级分类，返回给前端
    @Override
    public List<CategoryEntity> getLevel1Categorys() {
//        long l = System.currentTimeMillis();
        List<CategoryEntity> categoryEntities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
//        System.out.println("消耗时间："+(System.currentTimeMillis() - l));
        return categoryEntities;
    }


    // todo 产生堆外内存溢出  --》 内存没有得到释放
    // 解决方案 1升级lettuce  2使用jedis
    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        // 给缓存中方JSON字符串，拿出的JSON字符串，还用逆转为能用的对象类型 --》 序列化与反序列话

        /**
         * 1、空结果缓存 缓存穿透
         * 2、设置过期时间 缓存雪崩
         * 3、加锁 缓存击穿
         */


        // 1、加入缓存逻辑
        // JSON跨语言，跨平台兼容性好
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isEmpty(catalogJSON)) {
            System.out.println("缓存不命中，查数据库");
            // 2、缓存中没有，查询数据库
            Map<String, List<Catelog2Vo>> catalogJsonFromDb = getCatalogJsonFromDb();

            return catalogJsonFromDb;
        }
        System.out.println("缓存命中");

        // 转为指定的对象
        Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>(){});
        return result;

    }

    // 从数据库查询并封装分类数据
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDb() {
//    @Override
//    public Map<String, List<Catelog2Vo>> getCatalogJson() {


        // 同步代码块 只需要一把锁，就能锁住需要这个锁的所有线程
        // TODO 本地锁
        synchronized (this){
            String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
            if (!StringUtils.isEmpty(catalogJSON)) {
                // 缓存不为空，直接返回
                // 转为指定的对象
                Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>(){});
                return result;
            }
            System.out.println("查询数据库");
            /**
             *
             * 1、将数据的多次查询变为一次
             */
            List<CategoryEntity> selectList = baseMapper.selectList(null);


            // 1，查出所有一级分类
            List<CategoryEntity> level1Categorys = getParent_cid(selectList, 0L);

            // 2，封装数据
            Map<String, List<Catelog2Vo>> parent_cid = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
                // 1每一个的一级分类，查到这个一级分类的所有二级分类
                List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());
                // 2封装上面的指定结果
                List<Catelog2Vo> catelog2Vos = null;
                if (categoryEntities != null) {
                    catelog2Vos = categoryEntities.stream().map(l2 -> {
                        Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                        // 1 找当前二级分类的三级分类封装成VO
                        List<CategoryEntity> level3Catelog = getParent_cid(selectList, l2.getCatId());
                        if (level3Catelog != null) {
                            List<Catelog2Vo.CateLog3Vo> collect = level3Catelog.stream().map(l3 -> {
                                // 2 封装成指定格式
                                Catelog2Vo.CateLog3Vo cateLog3Vo = new Catelog2Vo.CateLog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());

                                return cateLog3Vo;
                            }).collect(Collectors.toList());
                            catelog2Vo.setCatalog3List(collect);
                        }

                        return catelog2Vo;
                    }).collect(Collectors.toList());
                }

                return catelog2Vos;
            }));

            // todo 3、将查到的数据放入缓存, 将对象装换为json放入缓存中
            String s = JSON.toJSONString(parent_cid);
            redisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);
            System.out.println("1111111111");
            return parent_cid;
        }

    }

    private List<CategoryEntity> getParent_cid(List<CategoryEntity> selectList, Long parent_cid) {
//        return baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId()));
        List<CategoryEntity> collect = selectList.stream().filter(item -> {
            return item.getParentCid() == parent_cid;
        }).collect(Collectors.toList());
        return collect;
    }

    private List<Long> findParentPath(Long catelogId, List<Long> paths) {
        // 1 手机当前节点id
        paths.add(catelogId);
        CategoryEntity byId = this.getById(catelogId);
        if (byId.getParentCid() != 0) {
            findParentPath(byId.getParentCid(), paths);
        }
        return paths;
    }

    // 递归查找所有菜单的子菜单
    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all) {

        return all.stream().filter(categoryEntity -> {
           return categoryEntity.getParentCid().equals(root.getCatId());/////////////////////////////////////////
        }).peek(categoryEntity -> {
            // 1、找到子菜单
            categoryEntity.setChildren(getChildrens(categoryEntity, all));
        }).sorted(Comparator.comparingInt(menu -> (menu.getSort() == null ? 0 : menu.getSort()))).collect(Collectors.toList());
    }

}