package com.vuestart.shushan.product.feign;

import com.vuestart.common.to.es.SkuEsModel;
import com.vuestart.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("shushan-search")
public interface SearchFeignService {
    @PostMapping( "/search/save/product")
    R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels);
}
