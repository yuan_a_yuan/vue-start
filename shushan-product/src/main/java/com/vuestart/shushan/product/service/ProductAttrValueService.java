package com.vuestart.shushan.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 15:58:32
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveProductAttr(List<ProductAttrValueEntity> collect);

    List<ProductAttrValueEntity> baseAttrListForSpu(Long spuId);


    void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> entities);


}

