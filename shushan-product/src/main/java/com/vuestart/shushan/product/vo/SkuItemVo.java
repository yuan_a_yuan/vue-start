package com.vuestart.shushan.product.vo;

import com.vuestart.shushan.product.entity.SkuImagesEntity;
import com.vuestart.shushan.product.entity.SkuInfoEntity;
import com.vuestart.shushan.product.entity.SpuInfoDescEntity;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @创建：--> DKD <--
 * @日期：2021-04-20 17:00
 */
@Data
public class SkuItemVo {
    // 1、sku基本信息获取 pms_sku_info
    SkuInfoEntity info;

    // 2、sku的图片信息
    List<SkuImagesEntity> images;

    // 3、获取spu销售属性组合
    List<SkuItemSaleAttrVo> saleAttr;

    // 4、获取spu的介绍
    SpuInfoDescEntity desp;

    // 5、获取spu规格参数信息
    List<SpuItemAttrGroupVo> groupAttrs;

    // 有货无货
    boolean hasStock = true;


    // 3、获取spu销售属性组合
//    @Data
//    public static class SkuItemSaleAttrVo {
//        private Long attrId;
//        private String attrName;
//        private List<String> attrValues;
//    }

//    @ToString
//    @Data
//    public static class SpuItemAttrGroupVo {
//        private String groupName;
//        List<SpuBaseAttrVo> attrs;
//    }

//    @ToString
//    @Data
//    public static class SpuBaseAttrVo {
//        private String attrName;
//        private String attrValue;
//    }

}
