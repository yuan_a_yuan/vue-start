package com.vuestart.shushan.product;

//import com.aliyun.oss.OSSClient;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.org.apache.bcel.internal.generic.LNEG;
import com.vuestart.shushan.product.dao.AttrGroupDao;
import com.vuestart.shushan.product.dao.SkuSaleAttrValueDao;
import com.vuestart.shushan.product.entity.BrandEntity;
import com.vuestart.shushan.product.service.BrandService;
import com.vuestart.shushan.product.service.CategoryService;
import com.vuestart.shushan.product.vo.SkuItemSaleAttrVo;
import com.vuestart.shushan.product.vo.SkuItemVo;
import com.vuestart.shushan.product.vo.SpuItemAttrGroupVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/*
* 1、引入oss-starter
* 2、配置key、endpoint等相关信息
* 3、使用OSSClient进行相关操作
* */

@Slf4j
@SpringBootTest
class ShushanProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    AttrGroupDao attrGroupDao;

    @Autowired
    SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Test
    public void test() {
//        List<SpuItemAttrGroupVo> attrGroupWithAttrsBySpuId = attrGroupDao.getAttrGroupWithAttrsBySpuId(11L, 225L);
//        System.out.println(attrGroupWithAttrsBySpuId);
        List<SkuItemSaleAttrVo> saleAttrsBySpuId = skuSaleAttrValueDao.getSaleAttrsBySpuId(9L);
        System.out.println(saleAttrsBySpuId);
    }

//    // 测试redis
//    @Resource
//    StringRedisTemplate stringRedisTemplate;
//    @Test
//    public void teststringRedisTemplate() {
//        // hello world
//        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
//
//        // 保存
//        ops.set("hello", "world" + UUID.randomUUID().toString());
//
//        // 查询
//        String hello = ops.get("hello");
//        System.out.println(hello);
//    }


    @Test
    public void testFindPath() {
        Long[] catelogPath = categoryService.findCatelogPath(225L);
        log.info("完整路径：{}", Arrays.asList(catelogPath));
    }

//    @Autowired
//    OSSClient ossClient;
//
//    @Test
//    public void testUpload() throws FileNotFoundException {
////        // Endpoint以杭州为例，其它Region请按实际情况填写。
////        String endpoint = "oss-cn-chengdu.aliyuncs.com";
////// 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
////        String accessKeyId = "LTAI4GCdetSn11Ww8oXSPoY8";
////        String accessKeySecret = "dfcjaxT338g59Sf41l8d4ZbQMwJiIi";
////
////// 创建OSSClient实例。
////        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
//
//// 上传文件流。
//        InputStream inputStream = new FileInputStream("E:\\Picture\\banner.jpg");
//
//        ossClient.putObject("shushan-hello", "banner.jpg", inputStream);
//
//// 关闭OSSClient。
//        ossClient.shutdown();
//
//        System.out.println("上传完成");
//    }

    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();

        /*brandEntity.setName("华为");
        brandService.save(brandEntity);
        System.out.println("保存成功。。。。");*/

        /*brandEntity.setBrandId(1L);
        brandEntity.setDescript("华为");
        brandService.updateById(brandEntity);*/

        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id",1L));
        list.forEach(System.out::println);
    }
}
