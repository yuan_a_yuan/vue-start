package com.vuestart.shushan.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vuestart.common.utils.PageUtils;
import com.vuestart.shushan.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 17:57:39
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

