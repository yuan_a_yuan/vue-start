package com.vuestart.shushan.coupon.dao;

import com.vuestart.shushan.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 17:57:39
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
