package com.vuestart.shushan.coupon.dao;

import com.vuestart.shushan.coupon.entity.SeckillSessionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动场次
 * 
 * @author dkd
 * @email m18275258687@163.com
 * @date 2021-01-29 17:57:39
 */
@Mapper
public interface SeckillSessionDao extends BaseMapper<SeckillSessionEntity> {
	
}
