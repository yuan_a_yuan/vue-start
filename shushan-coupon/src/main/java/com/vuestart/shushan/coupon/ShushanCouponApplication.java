package com.vuestart.shushan.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/*
* 1、如何使用nacos作为配置中心统一管理配置
*   1）引入依赖
*       <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
*   2）创建一个bootstrap.properties
*       spring.application.name=shushan-coupon
        spring.cloud.nacos.config.server-addr=127.0.0.1:8848
    3）需要给配置中心默认添加一个 数据集（Data Id） shushan-coupon.properties --》 默认规则： 应用名.properties
    4）给 应用名.properties 添加任何配置
    5）动态获取配置。
    *   @RefreshScope 动态刷新配置
    *   @Value("${coupon.user.name}") //获取properties里存储的值
    *   如果配置中心和当前应用配置文件中都配置了相同的项，优先使用配置中心的配置
    *
* 2、细节
*   1）命名空间：配置隔离
*       默认是 public（保留空间）；默认新增的所有配置都在public空间。
*       。：开发、测试、生产。利用命名空间来做环境隔离。
*           注意：在bootstrap.properties；配置上，需要使用哪个命名空间下的配置。
*               spring.cloud.nacos.config.namespace=9299b1f0-0344-4580-94a3-431cccf252f7
*       。。：每一个微服务互相隔离配置，每一个微服务都创建自己的命名空间，只加载自己命名空间下的所有配置。
*   2）配置集：所有的配置的集合
*
*   3）配置集Id：类似文件名
*       Data Id类似文件名
*
*   4）配置分组
*       默认所有的配置集都属于 DEFAULT_GROUP；
*       11.11；6.18；12.12；
*
* 3、同时加载多个配置集
*   1)微服务任何配置信息，任何配置文件都可放在配置中心中
*   2）只需要在bootstrap.properties中说明加载配置中心哪些文件即可
*   3）@Value、@ConfigurationProperties。。。
*       以前SpringBoot任何方法从配置文件中获取值，都能使用。配置中心有的优先使用配置中心的。
*
*
* 每个微服务创建自己的命名空间，使用配置分组来区分环境，dev、test、prop
* */

@EnableDiscoveryClient
@SpringBootApplication
public class ShushanCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShushanCouponApplication.class, args);
    }

}
