package com.vuestart.shushan.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ShushanThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShushanThirdPartyApplication.class, args);
    }

}
